package com.tk2323.ftsm.lab_ui_intent_a162021;

import android.content.Intent;
import android.net.Uri;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class EmailActivity extends AppCompatActivity {

    EditText etEmail;
    TextView tvMessage;
    ImageButton btnEmail, btnCall, btnWeb;

    int quantity;
    String name, email, message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email);

        etEmail = findViewById(R.id.email_et_email);
        tvMessage = findViewById(R.id.email_tv_message);
        btnEmail = findViewById(R.id.email_img_btn_email);
        btnCall = findViewById(R.id.email_img_btn_call);
        btnWeb = findViewById(R.id.email_img_btn_web);

        Intent intent = getIntent();

        quantity = intent.getIntExtra("Quantity", 0);
        name = intent.getStringExtra("Name");
        email = intent.getStringExtra("Email");

        etEmail.setText(email);
        message = "Hi, " + name + ", Thanks for visiting Waffle House. \n" + "You have ordered " + quantity + " Butter Waffles.";
        tvMessage.setText(message);

        btnEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setDataAndType(Uri.parse("mailto:"), "text/plain");

                emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {email});
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Hi, this is a receipt from Waffle House");
                emailIntent.putExtra(Intent.EXTRA_TEXT, message);

                startActivity(emailIntent);
            }
        });

        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:0123456789"));

                startActivity(callIntent);
            }
        });

        btnWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent webIntent = new Intent(Intent.ACTION_VIEW);
                webIntent.setData(Uri.parse("http://www.google.com"));

                startActivity(webIntent);
            }
        });
    }
}
