package com.tk2323.ftsm.lab_ui_intent_a162021;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button btnAdd, btnMinus, btnCheckout;
    TextView tvQuantity;
    EditText edName, edEmail;

    int quantity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnAdd = findViewById(R.id.main_btn_add);
        btnMinus = findViewById(R.id.main_btn_minus);
        btnCheckout = findViewById(R.id.main_btn_checkout);

        tvQuantity = findViewById(R.id.main_tv_quantity);

        edName = findViewById(R.id.main_et_name);
        edEmail = findViewById(R.id.main_et_email);

        quantity = 0;
        tvQuantity.setText(Integer.toString(quantity));

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                quantity++;
                tvQuantity.setText(Integer.toString(quantity));
            }
        });

        btnMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (quantity > 0)
                quantity--;
                tvQuantity.setText(Integer.toString(quantity));
            }
        });

        btnCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Hi " + edName.getText() + " (" + edEmail.getText() + "). You have ordered " + quantity + " Butter Waffles. Thank You!", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(MainActivity.this, EmailActivity.class);
                intent.putExtra("Name", edName.getText().toString());
                intent.putExtra("Email", edEmail.getText().toString());
                intent.putExtra("Quantity", quantity);
                startActivity(intent);
            }
        });
    }
}
